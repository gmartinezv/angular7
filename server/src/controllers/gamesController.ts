import { Request, Response }  from 'express';

import pool  from '../database';


class GamesController {


  public  index (req : Request, res: Response) {
        
           // res.send('Hello games controller con pool DB' );
            pool.query('select * from juegos');
            res.json(' juegos 2 ');

          }

  public  async list  (req : Request, res: Response) {                    
             const juegos = await pool.query('select * from juegos');
             res.json(juegos);
 
           }          

 public async  create (req : Request, res: Response):  Promise<void> {                    
             await pool.query(" insert into juegos set ?" , [req.body]);
             console.log(req.body); 
             res.json({texto:' juegos 2: creando un juego ahora si guardoó '});
 
           }          

public  async delete  (req : Request, res: Response) : Promise<void> {                    
              const {id} = req.params;
              const juegos = await pool.query("delete from juegos where id =?", [id]);
               res.json({texto:' eliminando :  un juego el ' + id});

          }                     
public async  update  (req : Request, res: Response) : Promise<void>{                    
                  const {id} = req.params;
                  const juegos = await pool.query("update juegos set ? where id =?", [req.body, id]);
                 res.json({texto:' actualizando :  un juego ' + id });

          }                               

 public async getOne (req : Request, res: Response) : Promise<any> {                    
            const {id} = req.params;
            const juegos = await pool.query("select * from juegos where id =?", [id]);
            if (juegos.length >0){
              return res.json(juegos[0]);
            }
            res.status(404).json({texto:' el juego ' + req.params.id + '  no existe ' });

          }                                         

    
}
export const gamesController = new GamesController();

