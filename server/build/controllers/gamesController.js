"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class GamesController {
    index(req, res) {
        // res.send('Hello games controller con pool DB' );
        database_1.default.query('select * from juegos');
        res.json(' juegos 2 ');
    }
    list(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const juegos = yield database_1.default.query('select * from juegos');
            res.json(juegos);
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield database_1.default.query(" insert into juegos set ?", [req.body]);
            console.log(req.body);
            res.json({ texto: ' juegos 2: creando un juego ahora si guardoó ' });
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const juegos = yield database_1.default.query("delete from juegos where id =?", [id]);
            res.json({ texto: ' eliminando :  un juego el ' + id });
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const juegos = yield database_1.default.query("update juegos set ? where id =?", [req.body, id]);
            res.json({ texto: ' actualizando :  un juego ' + id });
        });
    }
    getOne(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const juegos = yield database_1.default.query("select * from juegos where id =?", [id]);
            if (juegos.length > 0) {
                return res.json(juegos[0]);
            }
            res.status(404).json({ texto: ' el juego ' + req.params.id + '  no existe ' });
        });
    }
}
exports.gamesController = new GamesController();
