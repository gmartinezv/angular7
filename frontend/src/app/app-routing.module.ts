import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListJuegosComponent}  from './components/list-juegos/list-juegos.component';
import {  FormJuegosComponent}  from './components/form-juegos/form-juegos.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/juegos',
    pathMatch : 'full'
  },
  {
    path: 'juegos',
    component : ListJuegosComponent
  },
  {
    path: 'juegos/add',
    component : FormJuegosComponent

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
