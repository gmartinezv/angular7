import { Injectable } from '@angular/core';
import { HttpClient}  from '@angular/common/http';

import { Juego }  from '../models/Juego';
import { Observable }  from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JuegosService {

   API_URI = 'http://localhost:3000/api';

  constructor(private  http: HttpClient) { }

  getJuegos(){
    return this.http.get(`${this.API_URI}/games`);

  }

  getJuego(id:string){
    return this.http.get(`${this.API_URI}/games/${id}`);
  }

  deleteJuego(id:string){
    return this.http.delete(`${this.API_URI}/games/${id}`);
  }

  updateJuego(id:string, updatejuego : Juego) : Observable<any> {
    return this.http.put(`${this.API_URI}/games/${id}`, updatejuego);
  }




  saveJuego(juego : Juego){
    return this.http.post(`${this.API_URI}/games`, juego);

  }


}
