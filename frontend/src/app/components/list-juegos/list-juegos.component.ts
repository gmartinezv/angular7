import { Component, OnInit } from '@angular/core';
import { JuegosService } from '../../services/juegos.service';
import { Juego } from 'src/app/models/Juego';

@Component({
  selector: 'app-list-juegos',
  templateUrl: './list-juegos.component.html',
  styleUrls: ['./list-juegos.component.css']
})
export class ListJuegosComponent implements OnInit {
   juego : any = [];
  constructor(private juegosService: JuegosService) { }

  ngOnInit(): void {
    this.juegosService.getJuegos().subscribe(
         res => { // console.log(res),
        this.juego = res;
         },
        err => console.error(err)

    )

  }

}
