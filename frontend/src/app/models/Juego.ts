export interface Juego {

  id?: number;
  titulo: string;
  descripcion: string;
  foto?: string;
  created_at: Date;

}
