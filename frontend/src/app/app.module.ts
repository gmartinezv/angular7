import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule, HttpClient } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavegacionComponent } from './components/navegacion/navegacion.component';
import { FormJuegosComponent } from './components/form-juegos/form-juegos.component';
import { ListJuegosComponent } from './components/list-juegos/list-juegos.component';

import { JuegosService}  from './services/juegos.service';
@NgModule({
  declarations: [
    AppComponent,
    NavegacionComponent,
    FormJuegosComponent,
    ListJuegosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    JuegosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
